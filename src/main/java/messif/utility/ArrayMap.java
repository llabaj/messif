/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.utility;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * This is an IMMUTABLE implementation of the standard Map interface using two static fields with keys
 *  and with values. When searching, the list of keys is iterated over
 * 
 * @author david
 * @param <K> type of the key
 * @param <V> type of the value
 */
public class ArrayMap<K,V> extends AbstractMap<K, V> implements Serializable {
    
    /** Class id for serialization. */
    private static final long serialVersionUID = 1L;    

    /** Fixed list of keys. */
    private final K [] keys;
    
    /** Fixed list of values. */
    private final V [] values;

    /**
     * Creates new instance of this map given the list of keys and values. The passed arrays are 
     *  shallow copied.
     * @param keys list of keys
     * @param values list of values
     */
    public ArrayMap(K[] keys, V[] values) {
        this(keys, values, true);
    }

    /**
     * Creates new instance of this map given the list of keys and values. The passed arrays are 
     *  shallow copied, if the third argument is true.
     * @param keys list of keys
     * @param values list of values
     * @param copyArrays if true, the passed arrays are  shallow copied
     */
    public ArrayMap(K[] keys, V[] values, boolean copyArrays) {
        if (keys.length != values.length) {
            throw new IllegalArgumentException("the length of the lists of keys and values must be the same");
        }
        if (copyArrays) {
            this.keys = Arrays.copyOf(keys, keys.length);
            this.values = Arrays.copyOf(values, values.length);
        } else {
            this.keys = keys;
            this.values = values;
        }
    }
    
    /**
     * Creates this fixed map given existing map.
     * @param map existing key-value map
     */
    public ArrayMap(Map<K, V> map) {
        this.keys = (K[]) new Object [map.size()];
        this.values = (V[]) new Object [map.size()];
        
        int i = 0;
        for (Entry<K, V> next : map.entrySet()) {
            keys[i] = next.getKey();
            values[i] = next.getValue();
            i++;
        }
    }
    
    /**
     * Implementation of this method is the only necessary to fulfill the AbstactMap contract. All
     *  the other methods are for efficiency reasons only.
     * @return 
     */
    @Override
    public Set<Entry<K, V>> entrySet() {
        
        return new AbstractSet<Entry<K, V>>() {

            @Override
            public Iterator<Entry<K, V>> iterator() {
                return new Iterator<Entry<K, V>>() {

                    private int i = 0;
                    
                    @Override
                    public boolean hasNext() {
                        return i < keys.length;
                    }

                    @Override
                    public Entry<K, V> next() {
                        final K key = keys[i];
                        final V value = values[i];
                        try {
                            return new Entry<K, V>() {
                                @Override
                                public K getKey() {
                                    return key;
                                }
                                @Override
                                public V getValue() {
                                    return value;
                                }
                                @Override
                                public V setValue(V value) {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }
                            };
                        } finally {
                            i ++;
                        }
                    }

                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("Remove is not supported.");
                    }
                };
            }

            @Override
            public int size() {
                return keys.length;
            }
        };
    }

    @Override
    public Set<K> keySet() {
        return new AbstractSet<K>() {
            @Override
            public Iterator<K> iterator() {
                return new Iterator<K>() {

                    private int i = 0;
                    
                    @Override
                    public boolean hasNext() {
                        return i < keys.length;
                    }
                    @Override
                    public K next() {
                        try {
                            return keys[i];
                        } finally {
                            i ++;
                        }
                    }
                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException("Remove is not supported.");
                    }
                };
            }
            @Override
            public int size() {
                return keys.length;
            }
        };
    }
    
    @Override
    public V get(Object key) {
        if (key == null) {
            for (int i = 0; i < keys.length; i++) {
                if (keys[i] == null) {
                    return values[i];
                }
            }
        } else {
            for (int i = 0; i < keys.length; i++) {
                if (key.equals(keys[i])) {
                    return values[i];
                }
            }
        }
        return null;
    }

    @Override
    public boolean containsKey(Object key) {
        if (key == null) {
            for (K key1 : keys) {
                if (key1 == null) {
                    return true;
                }
            }
        } else {
            for (K key1 : keys) {
                if (key.equals(key1)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public V put(K key, V value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public V remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return keys.length;
    }
 
    /** 
     * Print the map as a JSON object.
     * @return 
     */
    @Override
    public String toString() {
        Iterator<Entry<K,V>> i = entrySet().iterator();
        if (! i.hasNext())
            return "{}";

        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (;;) {
            Entry<K,V> e = i.next();
            K key = e.getKey();
            V value = e.getValue();
            sb.append('"').append(key   == this ? "(this Map)" : key).append('"');
            sb.append(": ");
            sb.append(value == this ? "(this Map)" : value);
            if (! i.hasNext())
                return sb.append('}').toString();
            sb.append(',').append(' ');
        }
    }
    
}
