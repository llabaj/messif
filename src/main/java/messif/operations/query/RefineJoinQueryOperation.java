/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import messif.operations.AbstractOperation;
import messif.operations.GetJoinCandidatesOperation;
import messif.operations.OperationErrorCode;

/**
 * This operation refines of the encapsulated {@link JoinQueryOperation} using
 *  the candidate set stored in the provided {@link GetJoinCandidatesOperation}.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
@AbstractOperation.OperationName("query to refine candidate sets for similarity self join")
public class RefineJoinQueryOperation extends AbstractOperation {

    /** Class id for serialization. */
    private static final long serialVersionUID = 542401L;
    /**
     * Operation encapsulating the candidate set.
     */
    private final GetJoinCandidatesOperation candidateOperation;
    
    /**
     * Operation to be refined using the given candidate set.
     */
    private final JoinQueryOperation operationToRefine;
    
    /**
     * Creates the operation given a {@link JoinQueryOperation} to be refined and the candidate set
     * within a {@link GetJoinCandidatesOperation}.
     *
     * @param candidateOperation encapsulation of the candidate set
     * @param operationToRefine similarity join operation to be refined
     */
    public RefineJoinQueryOperation(GetJoinCandidatesOperation candidateOperation, JoinQueryOperation operationToRefine) {
        super();
        this.candidateOperation = candidateOperation;
        this.operationToRefine = operationToRefine;
        copyParameters(operationToRefine, true);        
    }
    
    /**
     * Get the candidate operation.
     * @return the candidate operation.
     */
    public GetJoinCandidatesOperation getCandidateOperation() {
        return candidateOperation;
    }
    
    /**
     * Returns the ranking operation to be refined.
     * @return the ranking operation to be refined.
     */
    public JoinQueryOperation getJoinQueryOperation() {
        return operationToRefine;
    }
    
    // *****************************        Overrides         ********************************* //
    
    @Override
    public Object getArgument(int index) throws IndexOutOfBoundsException {
        switch (index) {
        case 0:
            return getJoinQueryOperation().getMu();
        case 1:
            return getJoinQueryOperation().getK();
        default:
            throw new IndexOutOfBoundsException(RefineJoinQueryOperation.class.getName() + " has only two arguments");
        }
    }

    @Override
    public int getArgumentCount() {
        return 2;
    }
    
    @Override
    public boolean wasSuccessful() {
        return isErrorCode(OperationErrorCode.RESPONSE_RETURNED);
    }

    @Override
    public void endOperation() {
        endOperation(OperationErrorCode.RESPONSE_RETURNED);
    }

}
