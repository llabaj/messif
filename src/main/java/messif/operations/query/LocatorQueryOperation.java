/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import java.util.Collections;
import java.util.Set;
import messif.operations.AnswerType;
import messif.operations.RankingQueryOperation;

/**
 * Abstract class for query operations that have query identified just by its locator.
 * 
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 */
public abstract class LocatorQueryOperation extends RankingQueryOperation {
    
    /** Class serial id for serialization. */
    private static final long serialVersionUID = 3456012L;
    
    /**
     * Query object locators.
     */
    protected final Set<String> locators;


    //****************** Constructors ******************//

    /**
     * Creates a new instance of LocatorQueryOperation for a given query locator string and maximal 
     * number of objects to return.
     *
     * @param locators set of locators of query objects for which the query-by example should be solved
     * @param maxAnswerSize max number of objects in the answer
     * @param answerType the type of objects this operation stores in its answer
     */
    protected LocatorQueryOperation(Set<String> locators, int maxAnswerSize, AnswerType answerType) {
        super(answerType, maxAnswerSize);
        if (locators.isEmpty()) {
            throw new IllegalArgumentException("the list of locators to search for cannot be empty");
        }
        this.locators = Collections.unmodifiableSet(locators);
    }

    public Set<String> getLocators() {
        return locators;
    }
 
    public int getNumberLocators() {
        return locators.size();
    }
    
}
