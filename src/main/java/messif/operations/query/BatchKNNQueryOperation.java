/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.operations.query;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import messif.objects.LocalAbstractObject;
import messif.operations.AbstractOperation;
import messif.operations.AnswerType;

/**
 * A batch of several K-nearest neighbors query operations encapsulated as a single operation.
 * 
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @author Honza Brázdil, janinko.g@gmail.com
 */
@AbstractOperation.OperationName("k-nearest neighbors query")
public class BatchKNNQueryOperation extends BatchRankingQueryOperation<KNNQueryOperation> {

    /** Class serial id for serialization */
    private static final long serialVersionUID = 2L;

    //****************** Constructors ******************//

    /**
     * Creates a list of {@link KNNQueryOperation} for all specified query objects.
     * @param queryObjects iterator over the query objects
     * @param k the number of nearest neighbors to retrieve for each operation
     */
    @AbstractOperation.OperationConstructor({"Query object", "Number of nearest objects"})
    public BatchKNNQueryOperation(Iterator<? extends LocalAbstractObject> queryObjects, int k) {
        this(queryObjects, k, AnswerType.NODATA_OBJECTS);
    }

    /**
     * Creates a list of {@link KNNQueryOperation} for all specified query objects.
     * @param queryObjects iterator over the query objects
     * @param k the number of nearest neighbors to retrieve for each operation
     * @param answerType the type of objects this operation stores in its answer
     */
    @AbstractOperation.OperationConstructor({"Query object", "Number of nearest objects", "Answer type"})
    public BatchKNNQueryOperation(Iterator<? extends LocalAbstractObject> queryObjects, int k, AnswerType answerType) {
        this(queryObjects, Integer.MAX_VALUE, k, answerType);
    }

    /**
     * Creates a list of {@link KNNQueryOperation} for part of specified query objects.
     * @param queryObjects iterator over the query objects
     * @param maxNQueries maximal number of query objects read from the iterator (can be {@code Integer.MAX_VALUE}
     * @param k the number of nearest neighbors to retrieve for each operation
     * @param answerType the type of objects this operation stores in its answer
     */
    @AbstractOperation.OperationConstructor({"Query object", "Number of nearest objects", "Store the meta-object subdistances?", "Answer type"})
    public BatchKNNQueryOperation(Iterator<? extends LocalAbstractObject> queryObjects, int maxNQueries, int k, AnswerType answerType) {
        super(constructKNNQueryies(queryObjects, maxNQueries, k, answerType), answerType);
    }
    
    private static List<KNNQueryOperation> constructKNNQueryies(Iterator<? extends LocalAbstractObject> queryObjects, int maxNQueries, int k, AnswerType answerType){
        List<KNNQueryOperation> operations = new ArrayList<>();
        int i = 0;
        while (queryObjects.hasNext() && i ++ < maxNQueries) {
            operations.add(new KNNQueryOperation(queryObjects.next(), k, answerType));
        }
        return operations;
    }

}
