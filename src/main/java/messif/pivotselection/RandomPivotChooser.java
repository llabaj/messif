/*
 *  This file is part of MESSIF library.
 *
 *  MESSIF library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MESSIF library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MESSIF library.  If not, see <http://www.gnu.org/licenses/>.
 */
package messif.pivotselection;

import java.io.Serializable;
import messif.objects.LocalAbstractObject;
import messif.objects.util.AbstractObjectIterator;
import messif.objects.util.AbstractObjectList;

/**
 * RandomPivotChooser provides the capability of selecting a random object from the whole bucket.
 * <p>
 * WARNING: THIS PIVOT CHOOSER DOES NOT SELECT THE PIVOTS UNIFORMLY!!!
 * </p>
 * @author Michal Batko, Masaryk University, Brno, Czech Republic, batko@fi.muni.cz
 * @author Vlastislav Dohnal, Masaryk University, Brno, Czech Republic, dohnal@fi.muni.cz
 * @author David Novak, Masaryk University, Brno, Czech Republic, david.novak@fi.muni.cz
 * @deprecated WARNING: THIS PIVOT CHOOSER DOES NOT SELECT THE PIVOTS UNIFORMLY!!!
 */
@Deprecated()
public class RandomPivotChooser extends AbstractPivotChooser implements Serializable {
    /** Class version id for serialization */
    private static final long serialVersionUID = 2L;
    
    private final boolean unique;
    
    /** 
     * Creates a new instance of RandomPivotChooser. Pivots selected by calling 
     *  {@link #selectPivot(int) } will be pairwise different.
     */
    public RandomPivotChooser() {
        this(true);
    }

    /** 
     * Creates a new instance of RandomPivotChooser
     * @param unique if true (default), the pivots selected by {@link #selectPivot(int) } 
     *  are pairwise different.
     */
    public RandomPivotChooser(boolean unique) {
        this.unique = unique;
    }
    
    /** Method for selecting pivots and appending to the list of pivots.
     * It is used in getPivot() function for automatic selection of missing pivots.
     *
     * Statistics are maintained automatically.
     * @param sampleSetIterator Iterator over the sample set of objects to choose new pivots from
     */
    @Override
    protected void selectPivot(int count, AbstractObjectIterator<? extends LocalAbstractObject> sampleSetIterator) {
        for (LocalAbstractObject o : AbstractObjectList.randomList(count, unique, sampleSetIterator))
            preselectedPivots.add(o);
    }
    
}
