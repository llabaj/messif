package test;

/**
 * Created by david on 29/09/16.
 */
public class TestMain {

    public static void main(String [] params) throws NoSuchFieldException, IllegalAccessException {
        int i = 3;
        Class classInt = int.class;
        Integer integer = new Integer(3);
        Class classInteger = Integer.class;

        if (classInt.isInstance(i)) {
            System.out.println("int is intance of Integer");
        }
        //System.out.println("printing casted value: " + (classInt.cast(integer)));
        //if (classInteger.isAssignableFrom(classInt)) {
        if (classInt.isAssignableFrom(classInteger)) {
            System.out.println("they are assignable");
        }

        if (classInt.equals(classInteger)) {
            System.out.println("they are equal");
        }

        if (classInt.isPrimitive() && classInt == classInteger.getField("TYPE").get(null)) {
            System.out.println("wow");
        }

        System.out.println("end");
    }
}
