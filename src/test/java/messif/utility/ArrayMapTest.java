/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messif.utility;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author david
 */
public class ArrayMapTest extends TestCase {
    
    public ArrayMapTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    private ArrayMap<String, Integer> createTestMap() {
        String [] keys = new String [] {"one", "two", "three", "four"};
        Integer [] values = new Integer [] { 1, 2, 3, 4};
        
        return new ArrayMap<>(keys, values);
    }

    private ArrayMap<String, Integer> createEmptyTestMap() {
        return new ArrayMap<>(Collections.EMPTY_MAP);
    }
    
    /**
     * Test of entrySet method, of class ArrayMap.
     */
    public void testEntrySet() {
        System.out.println("entrySet");
        ArrayMap<String, Integer> instance = createTestMap();
        Set<Map.Entry<String, Integer>> entrySet = instance.entrySet();
        
        Iterator<Map.Entry<String, Integer>> iterator = entrySet.iterator();
        assertEquals(4, entrySet.size());
        assertEquals("one", iterator.next().getKey());
        assertEquals("two", iterator.next().getKey());
        assertEquals("three", iterator.next().getKey());
        assertEquals("four", iterator.next().getKey());
    }

    /**
     * Test of keySet method, of class ArrayMap.
     */
    public void testKeySet() {
        System.out.println("keySet");
        ArrayMap<String, Integer> instance = createTestMap();
        Set<String> result = instance.keySet();
        assertEquals(4, result.size());
        
        Iterator<String> iterator = result.iterator();
        assertEquals("one", iterator.next());
        assertEquals("two", iterator.next());
        assertEquals("three", iterator.next());
        assertEquals("four", iterator.next());
        
        // empty instance
        instance = createEmptyTestMap();
        assertEquals(0, instance.keySet().size());
    }

    /**
     * Test of keySet method, of class ArrayMap.
     */
    public void testValueSet() {
        System.out.println("keySet");
        ArrayMap<String, Integer> instance = createTestMap();
        Collection<Integer> result = instance.values();
        assertEquals(4, result.size());
        
        Iterator<Integer> iterator = result.iterator();
        assertEquals(1, iterator.next().intValue());
        assertEquals(2, iterator.next().intValue());
        assertEquals(3, iterator.next().intValue());
        assertEquals(4, iterator.next().intValue());
        
        // empty instance
        instance = createEmptyTestMap();
        assertEquals(0, instance.values().size());
    }    
    
    /**
     * Test of get method, of class ArrayMap.
     */
    public void testGet() {
        System.out.println("get");
        ArrayMap<String, Integer> instance = createTestMap();
        assertEquals(1, instance.get("one").intValue());
        assertEquals(2, instance.get("two").intValue());
        assertEquals(3, instance.get("three").intValue());
        assertEquals(4, instance.get("four").intValue());
    }

    /**
     * Test of size method, of class ArrayMap.
     */
    public void testSize() {
        System.out.println("size");
        ArrayMap<String, Integer> instance = createTestMap();
        assertEquals(4, instance.size());
    }
    
}
